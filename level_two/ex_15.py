#!/usr/bin/env python3

# Question 15
# Level 2

# Question:
# Write a program that computes the value of a+aa+aaa+aaaa with a given digit as the value of a.
# Suppose the following input is supplied to the program:
# 9
# Then, the output should be:
# 11106

# Hints:
# In case of input data being supplied to the question, it should be assumed to be a console input.


number = input('Please enter a single number:\n')
result = 0

for i in range(1, 5):
    new_number = number * i  # number is a str here, it should produce a srquence of the same char
    result = int(new_number) + result

print(result)
