#!/usr/bin/env python3

# Question 12
# Level 2

# Question:
# Write a program, which will find all such numbers between 1000 and 3000 (both included) such
# that each digit of the number is an even number.
# The numbers obtained should be printed in a comma-separated sequence on a single line.

# Hints: In case of input data being supplied to the question, it should be assumed to be a
# console input.


# Naive solution from the prompt:
values = []
for i in range(1000, 3001):
    s = str(i)
    if int(s[0]) % 2 == 0 and int(s[1]) % 2 == 0 and int(s[2]) % 2 == 0 and int(s[3]) % 2 == 0:
        values.append(s)

print(",".join(values))


# A way to support numbers with more or less than 4 digits:
def hasAllEvenDigits(number):
    number = str(number)
    number_of_even_digits = 0
    expected_len = len(number)

    for char in number:
        if int(char) % 2 == 0:
            number_of_even_digits += 1
        else:
            break

    return (number_of_even_digits == expected_len)


if __name__ == '__main__':
    all_even_digits_number = []

    for n in range(1000, 3001):
        if hasAllEvenDigits(n):
            all_even_digits_number.append(str(n))

    print(", ".join(all_even_digits_number))
