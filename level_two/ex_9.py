#!/usr/bin/env python3

# Question 9
# Level 2

# Question:
# Write a program that accepts sequence of lines as input and prints the lines after making all
# characters in the sentence capitalized.
# Suppose the following input is supplied to the program:
# Hello world
# Practice makes perfect
# Then, the output should be:
# HELLO WORLD
# PRACTICE MAKES PERFECT

# Hints:
# In case of input data being supplied to the question, it should be assumed to be a console input.


def uppercase_sentences():
    sentences = []
    while True:
        raw_text = input("Enter a sentence or leave blank to end\n")
        if raw_text:
            sentences.append(raw_text)
        else:
            break

    [print(x.upper()) for x in sentences]


if __name__ == '__main__':
    uppercase_sentences()
