#!/usr/bin/env python3

# Question 17
# Level 2

# Question: Write a program that computes the net amount of a bank account based a transaction
# log from console input. The transaction log format is shown as following: D 100 W 200

# D means deposit while W means withdrawal. Suppose the following input is supplied to the
# program: D 300 D 300 W 200 D 100 Then, the output should be: 500

# Hints: In case of input data being supplied to the question,
# it should be assumed to be a console input.


total_deposit = 0
total_withdrawal = 0

transactions = input('Enter the transaction log:\n').split()

# We can assume the even indexes: 0, 2, 4, etc are the type of transaction.  And that the idx + 1
# is the value of the transaction.
pointer = 0
while pointer < len(transactions):
    transaction_type = transactions[pointer]
    transaction_value = int(transactions[pointer + 1])

    if transaction_type == 'W':
        total_withdrawal = total_withdrawal + transaction_value
    elif transaction_type == 'D':
        total_deposit = total_deposit + transaction_value

    pointer = pointer + 2  # 2 because we want to jump from even to even number.

print(total_deposit - total_withdrawal)
