#!/usr/bin/env python3


# Question 13
# Level 2

# Question:
# Write a program that accepts a sentence and calculate the number of letters and digits.
# Suppose the following input is supplied to the program:
# hello world! 123
# Then, the output should be:
# LETTERS 10
# DIGITS 3

# Hints:
# In case of input data being supplied to the question, it should be assumed to be a console input.


sentence = input("Enter a sentence with letters and digits:\n")
digits_count = 0
letters_count = 0

for c in sentence:
    if c.isdigit():
        digits_count = digits_count + 1
    elif c.isalpha():
        letters_count = letters_count + 1

print("LETTERS: " + str(letters_count))
print("DIGITS: " + str(digits_count))
