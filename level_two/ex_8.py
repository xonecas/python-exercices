#!/usr/bin/env python3

# Question 8
# Level 2

# Question:
# Write a program that accepts a comma separated sequence of words as input and prints the words
# in a comma-separated sequence after sorting them alphabetically.
# Suppose the following input is supplied to the program:
# without,hello,bag,world
# Then, the output should be:
# bag,hello,without,world

# Hints:
# In case of input data being supplied to the question, it should be assumed to be a console input.


def sort_words():
    words_raw = input('Please enter a comma separated list of words\n')
    words_list = words_raw.split(',')
    print(sorted(words_list))


if __name__ == '__main__':
    sort_words()
