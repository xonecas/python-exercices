#!/usr/bin/env python3


# Question 14
# Level 2

# Question:
# Write a program that accepts a sentence and calculate the number of upper case letters
# and lower case letters.
# Suppose the following input is supplied to the program:
# Hello world!
# Then, the output should be:
# UPPER CASE 1
# LOWER CASE 9

# Hints:
# In case of input data being supplied to the question, it should be assumed to be a console input.


sentence = input("Enter a sentence with upper and lower case letters:\n")
upper_count = 0
lower_count = 0

for c in sentence:
    if c.isupper():
        upper_count = upper_count + 1
    elif c.islower():
        lower_count = lower_count + 1

print("UPPER CASE", upper_count)
print("LOWER CASE", lower_count)
