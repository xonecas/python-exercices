#!/usr/bin/env python3

# Question 11
# Level 2

# Question:
# Write a program which accepts a sequence of comma separated 4 digit binary numbers as its input
# and then check whether they are divisible by 5 or not. The numbers that are divisible by 5 are
# to be printed in a comma separated sequence.
# Example:
# 0100,0011,1010,1001
# Then the output should be:
# 1010
# Notes: Assume the data is input by console.

# Hints:
# In case of input data being supplied to the question, it should be assumed to be a console input.


def binary_to_decimal(binary):
    """
    A simple approach to convert from binary.  This could have been done simpler thanks to python's
    int type:

        int(binary, 2)

    This function is equivalent to the line above.
    """
    decimal = 0
    for n in binary:
        decimal = decimal * 2 + int(n)

    return decimal


def divisible_by_five():
    numbers_raw = input('Enter a list of binary numbers, seperated by commas\n')
    success_numbers = []

    for n in numbers_raw.split(','):
        if binary_to_decimal(n) % 5 == 0:
            success_numbers.append(n)

    print(",".join(success_numbers))


if __name__ == '__main__':
    divisible_by_five()
